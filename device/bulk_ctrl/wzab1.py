# MicroPython USB WZab1 module
# MIT license; Copyright (c) 2024 Wojciech Zabolotny
# Based on examples from 
# https://github.com/projectgus/micropython-lib/tree/feature/usbd_python/micropython/usb
# MIT license; Copyright (c) 2023 Paul Hamshere, 2023-2024 Angus Gratton

import struct
from micropython import schedule
from usb.device.impl import Interface, Buffer

_EP_IN_FLAG = const(1 << 7)

class WZab1Interface(Interface):
    # Base class to implement a USB WZab1 device in Python.
    
    def __init__(self,rxlen=3000, txlen=3000):
        super().__init__()
        self.ep_out = None # RX direction (host to device)
        self.ep_in = None # TX direction (device to host)
        self.ep_c_out = None # RX direction (host to device)
        self.ep_c_in = None # TX direction (device to host)
        self._rx = Buffer(rxlen)
        self._tx = Buffer(txlen)
        self._rx_c = Buffer(rxlen)
        self._tx_c = Buffer(txlen)
    
    def _tx_xfer(self):
        # Keep an active IN transfer to send data to the host, whenever
        # there is data to send.
        if self.is_open() and not self.xfer_pending(self.ep_in) and self._tx.readable():
            self.submit_xfer(self.ep_in, self._tx.pend_read(), self._tx_cb)

    def _tx_cb(self, ep, res, num_bytes):
        #print(num_bytes,self._tx._n)
        if res == 0:
            self._tx.finish_read(num_bytes)
        self._tx_xfer()

    def _rx_xfer(self):
        # Keep an active OUT transfer to receive messages from the host
        if self.is_open() and not self.xfer_pending(self.ep_out) and self._rx.writable():
            self.submit_xfer(self.ep_out, self._rx.pend_write(), self._rx_cb)

    def _rx_cb(self, ep, res, num_bytes):
        #print("rx:"+str(num_bytes)+"\n")
        if res == 0:
            self._rx.finish_write(num_bytes)
            schedule(self._on_rx, ep)
        self._rx_xfer()

    def _tx_c_xfer(self):
        # Keep an active IN transfer to send data to the host, whenever
        # there is data to send.
        if self.is_open() and not self.xfer_pending(self.ep_c_in) and self._tx_c.readable():
            self.submit_xfer(self.ep_c_in, self._tx_c.pend_read(), self._tx_c_cb)

    def _tx_c_cb(self, ep, res, num_bytes):
        #print(num_bytes,self._tx_c._n)
        if res == 0:
            self._tx_c.finish_read(num_bytes)
        self._tx_c_xfer()

    def _rx_c_xfer(self):
        # Keep an active OUT transfer to receive messages from the host
        if self.is_open() and not self.xfer_pending(self.ep_c_out) and self._rx_c.writable():
            self.submit_xfer(self.ep_c_out, self._rx_c.pend_write(), self._rx_c_cb)

    def _rx_c_cb(self, ep, res, num_bytes):
        #print("rx:"+str(num_bytes)+"\n")
        if res == 0:
            self._rx_c.finish_write(num_bytes)
            schedule(self._on_rx_c, ep)
        self._rx_c_xfer()
   
    def _on_rx(self, ep):
        # Receive received data. Called via micropython.schedule, outside of the USB callback function.
        m = self._rx.pend_read()
        dt = bytes(m)        
        self._rx.finish_read(len(m))
        # Try to convert it to the JSON
        print(ep)
        # Send back confirmation:
        m = self._tx.write(b"In endpoint "+str(ep) + ", I have received:"+dt)
        self._tx_xfer()

    def _on_rx_c(self, ep):
        # Receive received data. Called via micropython.schedule, outside of the USB callback function.
        m = self._rx_c.pend_read()
        dt = bytes(m)        
        self._rx_c.finish_read(len(m))
        # Try to convert it to the JSON
        print(ep)
        # Send back confirmation:
        m = self._tx_c.write(b"C In endpoint "+str(ep) + ", I have received:"+dt)
        self._tx_c_xfer()

    def desc_cfg(self, desc, itf_num, ep_num, strs):
        strs.append("WZ1")
        desc.interface(itf_num, 4, iInterface = len(strs)-1)
        self.ep_out = ep_num
        self.ep_in = (ep_num) | _EP_IN_FLAG
        self.ep_c_out = ep_num+1
        self.ep_c_in = (ep_num+1) | _EP_IN_FLAG
        desc.endpoint(self.ep_out,"bulk",64,0)
        desc.endpoint(self.ep_in,"bulk",64,0)
        desc.endpoint(self.ep_c_out,"bulk",64,0)
        desc.endpoint(self.ep_c_in,"bulk",64,0)
        
    def num_itfs(self):
        return 1
        
    def num_eps(self):
        return 4

    def on_open(self):
        super().on_open()
        # kick off any transfers that may have queued while the device was not open
        self._tx_xfer()
        self._rx_xfer()
        self._tx_c_xfer()
        self._rx_c_xfer()
 
import usb.device
wz=WZab1Interface()                                                         
usb.device.get().init(wz, builtin_driver=True)
       
