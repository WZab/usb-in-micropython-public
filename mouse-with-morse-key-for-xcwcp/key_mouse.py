import usb
import machine as m
import usb.device.mouse as ms
import time
settling_time = 5 # for debouncing
p2=m.Pin(15,m.Pin.IN , m.Pin.PULL_UP)
mi=ms.MouseInterface()
mi.report_descriptor = bytes(
    [
        0x05,
        0x01,  # Usage Page (Generic Desktop)
        0x09,
        0x02,  # Usage (Mouse)
        0xA1,
        0x01,  # Collection (Application)
        0x09,
        0x01,  # Usage (Pointer)
        0xA1,
        0x00,  # Collection (Physical)
        0x05,
        0x09,  # Usage Page (Buttons)
        0x19,
        0x01,  # Usage Minimum (01),
        0x29,
        0x03,  # Usage Maximun (03),
        0x15,
        0x00,  # Logical Minimum (0),
        0x25,
        0x01,  # Logical Maximum (1),
        0x75,
        0x01,  # Report Size (1),
        0x95,
        0x03,  # Report Count (3),
        0x81,
        0x02,  # Input (Data, Variable, Absolute), ;3 button bits
        0x95,
        0x05,  # Report Count (5),
        0x75,
        0x01,  # Report Size (1) 5 bit padding
        0x81,
        0x03,  # Input (Constant), 
        0x05,
        0x01,  # Usage Page (Generic Desktop),
        0x09,
        0x30,  # Usage (X),
        0x09,
        0x31,  # Usage (Y),
        0x09,  # Added by WZab
        0x38,  # Usage (Wheel), added by WZab
        0x15,
        0x81,  # Logical Minimum (-127),
        0x25,
        0x7F,  # Logical Maximum (127),
        0x75,
        0x08,  # Report Size (8),
        0x95,
        0x02,  # Report Count (2),
        0x81,
        0x06,  # Input (Data, Variable, Relative), ;2 position bytes (X & Y)
        0xC0,  # End Collection,
        0xC0,  # End Collection
    ]
)

usb.device.get().init(mi, builtin_driver=True)
t2=m.Timer()
def t2_cb(t):
    mi.click_middle(1 - p2.value())   
def p2_cb(p):
    t2.init(mode=m.Timer.ONE_SHOT, period=settling_time, callback=t2_cb)
p2.irq(trigger=m.Pin.IRQ_RISING | m.Pin.IRQ_FALLING, handler=p2_cb)

