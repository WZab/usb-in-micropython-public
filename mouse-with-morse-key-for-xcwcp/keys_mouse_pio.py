import usb
import machine as m
import usb.device.mouse as ms
import time
import rp2

# Pins for a iambic key
p1=m.Pin(15,m.Pin.IN , m.Pin.PULL_UP)
p2=m.Pin(14,m.Pin.IN , m.Pin.PULL_UP)

# Pin for a straight key
p3=m.Pin(8,m.Pin.IN , m.Pin.PULL_UP)

# PIO base frequency
pio_freq = 10000

# The debouncing code is adapted from:  https://github.com/GitJer/Some_RPI-Pico_stuff/tree/main/Button-debouncer
@rp2.asm_pio(in_shiftdir=rp2.PIO.SHIFT_LEFT)
def debounce():
    wrap_target()
    jmp(pin,"isone")    #executed only once: is the pin currently 0 or 1?
    label("iszero")
    wait(1,pin,0)      # the pin is 0, wait for it to become 1
    set(x,31)          # prepare to test the pin for 31 times
    label("checkzero")
    jmp(pin,"stillone")  # check if the pin is still 1
    jmp("iszero")        # if the pin has returned to 0, start over
    label("stillone")
    jmp(x_dec,"checkzero") # decrease the time to wait, or the pin has definitively become 1
    set(y,0)
    in_(y,32)
    push()
    #irq(block,rel(0))
    label("isone")
    wait(0,pin,0)      # the pin is 1, wait for it to become 0
    set(x,31)          # prepare to test the pin for 31 times
    label("checkone")
    jmp(pin,"isone")    # if the pin has returned to 1, start over
    jmp(x_dec,"checkone")  # decrease the time to wait
    set(y,1)
    in_(y,32)
    push()
    #irq(block,rel(0))
    jmp("iszero")        # the pin has definitively become 0
    wrap()


mi=ms.MouseInterface()
mi.report_descriptor = bytes(
    [
        0x05,
        0x01,  # Usage Page (Generic Desktop)
        0x09,
        0x02,  # Usage (Mouse)
        0xA1,
        0x01,  # Collection (Application)
        0x09,
        0x01,  # Usage (Pointer)
        0xA1,
        0x00,  # Collection (Physical)
        0x05,
        0x09,  # Usage Page (Buttons)
        0x19,
        0x01,  # Usage Minimum (01),
        0x29,
        0x03,  # Usage Maximun (03),
        0x15,
        0x00,  # Logical Minimum (0),
        0x25,
        0x01,  # Logical Maximum (1),
        0x75,
        0x01,  # Report Size (1),
        0x95,
        0x03,  # Report Count (3),
        0x81,
        0x02,  # Input (Data, Variable, Absolute), ;3 button bits
        0x95,
        0x05,  # Report Count (5),
        0x75,
        0x01,  # Report Size (1) 5 bit padding
        0x81,
        0x03,  # Input (Constant), 
        0x05,
        0x01,  # Usage Page (Generic Desktop),
        0x09,
        0x30,  # Usage (X),
        0x09,
        0x31,  # Usage (Y),
        0x09,  # Added by WZab
        0x38,  # Usage (Wheel), added by WZab
        0x15,
        0x81,  # Logical Minimum (-127),
        0x25,
        0x7F,  # Logical Maximum (127),
        0x75,
        0x08,  # Report Size (8),
        0x95,
        0x02,  # Report Count (2),
        0x81,
        0x06,  # Input (Data, Variable, Relative), ;2 position bytes (X & Y)
        0xC0,  # End Collection,
        0xC0,  # End Collection
    ]
)


sm1 = rp2.StateMachine(0, debounce, freq=pio_freq, in_base=p1, jmp_pin=p1)
sm2 = rp2.StateMachine(1, debounce, freq=pio_freq, in_base=p2, jmp_pin=p2)
sm3 = rp2.StateMachine(2, debounce, freq=pio_freq, in_base=p3, jmp_pin=p3)

usb.device.get().init(mi, builtin_driver=True)

sm1.active(1)
sm2.active(1)
sm3.active(1)

#Loop handling the events
while True:
  if sm1.rx_fifo():
    mi.click_left(sm1.get()) 
  if sm2.rx_fifo():
    mi.click_right(sm2.get()) 
  if sm3.rx_fifo():
    mi.click_middle(sm3.get()) 

